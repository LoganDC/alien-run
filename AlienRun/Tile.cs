﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace AlienRun
{
    class Tile
    {

        public enum TileType
        {
            IMPASSABLE, // = 0, blocks player from any direction
            PLATFORM, // = 1, blocks player movement downward only
        }

        // -------------------------------------
        // Data
        // -------------------------------------
        private Texture2D sprite;
        private Vector2 position;
        private TileType type;


        // -------------------------------------
        // Behaviour
        // -------------------------------------
        public Tile (Texture2D newSprite, Vector2 newPosition, TileType newType)
        {
            sprite = newSprite;
            position = newPosition;
            type = newType;
        }
        // -------------------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(sprite, position, Color.White);
        }
        // -------------------------------------
        public Rectangle GetBounds()
        {
            return new Rectangle((int)position.X, (int)position.Y, sprite.Width, sprite.Height);
        }
        // -------------------------------------
        public TileType GetTileType()
        {
            return type;
        }
        // -------------------------------------

    }
}
