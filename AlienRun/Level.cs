﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System;

namespace AlienRun
{
    class Level
    {
        // -------------------------------------
        // Data
        // -------------------------------------
        private Tile[,] tiles;

        private Texture2D boxTexture;
        private Texture2D bridgeTexture;

        private const int LEVEL_WIDTH = 100;
        private const int LEVEL_HEIGHT = 100;

        private const int TILE_WIDTH = 70;
        private const int TILE_HEIGT = 70;

        // -------------------------------------
        // Behaviour
        // -------------------------------------
        public void LoadContent(ContentManager content)
        {
            // Creating a single copy of the tile texture that will be used by all tiles
            boxTexture = content.Load<Texture2D>("graphics/tiles/box");
            bridgeTexture = content.Load<Texture2D>("graphics/tiles/bridge");

            tiles = new Tile[LEVEL_WIDTH, LEVEL_HEIGHT];

            // Create solid boxes
            CreateBox(0, 8);
            CreateBox(1, 8);
            CreateBox(2, 8);
            CreateBox(3, 8);
            CreateBox(4, 8);
            CreateBox(5, 8);
            CreateBox(6, 8);
            CreateBox(7, 8);
            CreateBox(8, 8);

            // Create platforms
            CreateBridge(1, 4);
            CreateBridge(2, 4);
            CreateBridge(6, 4);
            CreateBridge(7, 4);
            CreateBridge(8, 4);
            CreateBridge(9, 4);
        }
        // -------------------------------------
        public void CreateBox(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_WIDTH);
            Tile newTile = new Tile(boxTexture, tilePosition, Tile.TileType.IMPASSABLE);
            tiles[tileX, tileY] = newTile;
        }

        public void CreateBridge(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_WIDTH);
            Tile newTile = new Tile(bridgeTexture, tilePosition, Tile.TileType.PLATFORM);
            tiles[tileX, tileY] = newTile;
        }
        // -------------------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            for (int x = 0; x < LEVEL_WIDTH; ++x)
            {
                for (int y = 0; y < LEVEL_HEIGHT; ++y)
                {
                    if (tiles[x, y] != null)
                        tiles[x, y].Draw(spriteBatch);
                }
            }
        }
        // -------------------------------------
        public List<Tile> GetTilesInBounds(Rectangle bounds)
        {
            // Create an empty list to fill with tiles
            List<Tile> tilesInBounds = new List<Tile>();

            // Determine the tile coordinate range for this rect
            int leftTile = (int)Math.Floor((float)bounds.Left / TILE_WIDTH);
            int rightTile = (int)Math.Ceiling((float)bounds.Right / TILE_WIDTH) - 1;
            int topTile = (int)Math.Floor((float)bounds.Top / TILE_HEIGT);
            int bottomTile = (int)Math.Ceiling((float)bounds.Bottom / TILE_HEIGT) - 1;

            // Loop through this range and add any tiles to the list
            for (int x = leftTile; x <= rightTile; ++x)
            {
                for (int y = topTile; y <= bottomTile; ++y)
                {
                    if (GetTile(x, y) != null)
                        tilesInBounds.Add(tiles[x, y]);
                }
            }

            return tilesInBounds;
        }
        // -------------------------------------
        public Tile GetTile(int x, int y)
        {
            // Check if we are out of bounds
            if (x < 0 || x >= LEVEL_WIDTH || y < 0 || y >= LEVEL_HEIGHT)
                return null;

            // Otherwise we are within the bounds
            return tiles[x, y];
        }
        // -------------------------------------
    }
}
